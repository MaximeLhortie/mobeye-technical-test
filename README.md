# Mobeye technical test solution

This project is based on `create-react-app` available via `npx`

# Contribution

## Requirements

In order to install source, you will need:

- Node JS >= 14.17
- npm >= 8.1

## Installation

To install sources and run the app, run command:

```sh
npm ci
```

## Run

To run the app:

```sh
npm start
```

And then you can access via: http://localhost:3000/

## Available scripts

There is saveral script avalable, here is the list:

Build:

```sh
npm run build
```

Eject:

```sh
npm run eject
```

Lint code:

```sh
npm run lint
```

Format code:

```sh
npm run format
```
