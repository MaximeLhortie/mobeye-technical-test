import React from "react";
import ReactDOM from "react-dom";

import App from "./app/views/App";

import "./static/css/index.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
