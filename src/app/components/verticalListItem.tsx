import React, { Component } from "react";

import SchoolItemStore from "../helpers/schoolItemStore";

import School from "../models/school";

type VerticalListItemComponentProps = {
  schoolObj: School;
};

class VerticalListItemComponent extends Component<VerticalListItemComponentProps> {
  constructor(props: VerticalListItemComponentProps) {
    super(props);
    this.state = {};
    this.onCheckboxClick = this.onCheckboxClick.bind(this);
  }

  onCheckboxClick(): void {
    const { schoolObj } = this.props;
    SchoolItemStore.toggleId(schoolObj.id);
  }

  render(): JSX.Element {
    const { schoolObj } = this.props;
    return (
      <div>
        <input type="checkbox" onChange={this.onCheckboxClick} /> {schoolObj.fields.nom_etablissement}
      </div>
    );
  }
}

export default VerticalListItemComponent;
