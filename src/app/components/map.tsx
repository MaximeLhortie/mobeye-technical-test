import React, { Component } from "react";
import { MapContainer, TileLayer } from "react-leaflet";

import MarkerComponent from "./marker";

import School from "../models/school";

import "../../static/css/components/map.css";

type MapComponentProps = {
  schoolList: School[];
};

class MapComponent extends Component<MapComponentProps> {
  constructor(props: MapComponentProps) {
    super(props);
    this.state = {};
  }

  render(): JSX.Element {
    const { schoolList } = this.props;
    return (
      <MapContainer className="leaflet-map-container" center={[48.857734, 2.339438]} zoom={12} scrollWheelZoom>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {schoolList.map(school => {
          return <MarkerComponent key={`map-pin-${school.id}`} schoolObj={school} />;
        })}
      </MapContainer>
    );
  }
}

export default MapComponent;
