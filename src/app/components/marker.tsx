import React, { Component } from "react";
import { Marker, Popup } from "react-leaflet";

import School from "../models/school";

type MarkerComponentProps = {
  schoolObj: School;
};

class MarkerComponent extends Component<MarkerComponentProps> {
  constructor(props: MarkerComponentProps) {
    super(props);
    this.state = {};
  }

  render(): JSX.Element {
    const { schoolObj } = this.props;
    return (
      <Marker position={[schoolObj.fields.position.lat, schoolObj.fields.position.lon]}>
        <Popup>
          {schoolObj.fields.nom_etablissement}
          <br />
          Nombre d&apos;élèves : {schoolObj.fields.nombre_d_eleves}
        </Popup>
      </Marker>
    );
  }
}

export default MarkerComponent;
