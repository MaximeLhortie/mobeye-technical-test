import React, { Component } from "react";

import VerticalListItemComponent from "./verticalListItem";

import SchoolItemStore from "../helpers/schoolItemStore";
import { exportToCSV, exportToJSON } from "../utils/export";

import School from "../models/school";

import "../../static/css/components/verticalList.css";

type VerticalListComponentProps = {
  schoolList: School[];
};

class VerticalListComponent extends Component<VerticalListComponentProps> {
  constructor(props: VerticalListComponentProps) {
    super(props);
    this.state = {};
    this.onCsvButonClick = this.onCsvButonClick.bind(this);
    this.onJsonButtonClick = this.onJsonButtonClick.bind(this);
    this.getSchoolsToExport = this.getSchoolsToExport.bind(this);
  }

  onCsvButonClick(): void {
    exportToCSV(this.getSchoolsToExport());
  }

  onJsonButtonClick(): void {
    exportToJSON(this.getSchoolsToExport());
  }

  getSchoolsToExport(): School[] {
    const { schoolList } = this.props;
    const storedSchoolsId = SchoolItemStore.getStoredSchoolsId();
    const schoolsToExport: School[] = [];
    storedSchoolsId.forEach(schoolId => {
      const schoolObj = schoolList.find(school => school.id === schoolId);
      if (schoolObj) {
        schoolsToExport.push(schoolObj);
      }
    });
    return schoolsToExport;
  }

  render(): JSX.Element {
    const { schoolList } = this.props;
    return (
      <div className="vertical-list">
        Export as{" "}
        <button type="button" onClick={this.onCsvButonClick}>
          CSV
        </button>{" "}
        <button type="button" onClick={this.onJsonButtonClick}>
          JSON
        </button>
        {schoolList.map(school => {
          return <VerticalListItemComponent key={`v-item-${school.id}`} schoolObj={school} />;
        })}
      </div>
    );
  }
}

export default VerticalListComponent;
