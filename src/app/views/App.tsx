import React, { Component } from "react";

import MapComponent from "../components/map";
import VerticalListComponent from "../components/verticalList";

import LocalStorage from "../helpers/localStorage";

import API from "../models/api";
import School from "../models/school";

import "../../static/css/views/App.css";

type AppState = {
  schoolList: School[];
};

class App extends Component<unknown, AppState> {
  constructor(props: unknown) {
    super(props);
    this.state = {
      schoolList: [],
    };
  }

  async componentDidMount(): Promise<void> {
    const schoolListResponse = await API.getSchoolList();
    LocalStorage.clear();
    this.setState({ schoolList: schoolListResponse });
  }

  render(): JSX.Element {
    const { schoolList } = this.state;
    return (
      <div className="App">
        <VerticalListComponent schoolList={schoolList} />
        <MapComponent schoolList={schoolList} />
      </div>
    );
  }
}

export default App;
