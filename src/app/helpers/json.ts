/* eslint-disable no-console */

/**
 * @description Used to read JSON file
 * @param json JSON to read
 * @param key JSON key to read
 * @return the value if value found or undefined
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
function readFromJSON<T>(json: any, key: string, errorWhenMissing = false): T | undefined {
  if (json === null || json === undefined) {
    console.debug(`trying to read ${key} from undefined`);
    return undefined;
  }
  const value = json[key];
  if (value === undefined) {
    // lowering log level because that actually happens a *lot*
    if (errorWhenMissing) {
      console.error(`${key} is missing from ${JSON.stringify(json, null, 2)}`);
    } else {
      console.debug(`${key} is missing from ${JSON.stringify(json, null, 2)}`);
    }
    return undefined;
  }
  return value;
}

export default readFromJSON;
