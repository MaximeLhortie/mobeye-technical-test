import LocalStorage from "./localStorage";

/**
 * @description Used to make school selection store easier
 */
const SchoolItemStore = {
  /**
   * @description (constant) Key used to store school id in localStorage
   */
  STORE_ID: "selected-v-items",

  /**
   * @description Used to store/remove a School ID in localStorage
   * @param schoolId School ID to store in localStorage
   */
  toggleId: (schoolId: string): void => {
    const selectedIds = LocalStorage.get(SchoolItemStore.STORE_ID)?.split(";") ?? [];
    if (SchoolItemStore.isIdExist(schoolId, selectedIds)) {
      SchoolItemStore.removeId(schoolId, selectedIds);
    } else {
      SchoolItemStore.saveId(schoolId, selectedIds);
    }
  },

  /**
   * @description Used to check if a School ID is in the given Schools ID list
   * @param schoolId School ID to check
   * @param selectedIds Schools ID list
   * @returns true if schoolId is in selectedIds, false otherwise
   */
  isIdExist: (schoolId: string, selectedIds: string[]): boolean => {
    return !!selectedIds.find(id => id === schoolId);
  },

  /**
   * @description Used to store a School ID in localStorage
   * @param schoolId School ID to store in localStorage
   * @param selectedIds Schools ID already in localStorage
   */
  saveId: (schoolId: string, selectedIds: string[]): void => {
    const updatedSelectedIds = selectedIds;
    updatedSelectedIds.push(schoolId);
    LocalStorage.set(SchoolItemStore.STORE_ID, `${updatedSelectedIds.join(";")}`);
  },

  /**
   * @description Used to remove a School ID in localStorage
   * @param schoolId School ID to remove in localStorage
   * @param selectedIds Schools ID already in localStorage
   */
  removeId: (schoolId: string, selectedIds: string[]): void => {
    const updatedSelectedIds = selectedIds;
    updatedSelectedIds?.splice(updatedSelectedIds.indexOf(schoolId), 1);
    if (updatedSelectedIds.length === 0) {
      LocalStorage.remove(SchoolItemStore.STORE_ID);
      return;
    }
    LocalStorage.set(SchoolItemStore.STORE_ID, updatedSelectedIds?.join(";"));
  },

  getStoredSchoolsId: (): string[] => {
    return LocalStorage.get(SchoolItemStore.STORE_ID)?.split(";") ?? [];
  },
};

export default SchoolItemStore;
