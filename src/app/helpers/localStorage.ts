/* eslint-disable no-console */
/**
 * @description Helper used to interact with localStorage
 */
const LocalStorage = {
  /**
   * @description Used to get a value in localStorage
   * @param key key to read in localStorage
   * @returns the value if value is found or undefined
   */
  get: (key: string): string | undefined => {
    const data = localStorage.getItem(key);
    if (data === null) {
      console.debug(`${key} is missing from localStorage`);
      return undefined;
    }
    return data;
  },
  /**
   * @description Used to store a key:value in localStorage
   * @param key key to set in localStorage
   * @param value value to store in localStorage
   */
  set: (key: string, value: string): void => {
    localStorage.setItem(key, value);
  },
  /**
   * @description Used to remove a key:value in localStorage
   * @param key key to remove in localStorage
   */
  remove: (key: string): void => {
    localStorage.removeItem(key);
  },
  /**
   * @description Used to clear localStorage
   */
  clear: (): void => {
    localStorage.clear();
  },
};

export default LocalStorage;
