/* eslint-disable no-console */

import readFromJSON from "../helpers/json";

import School from "./school";

interface RequestParamsHelper<T, TCatch> {
  url: string;
  name: string;
  requestOptions?: RequestInit;
  success: (result: Response) => T;
  failure: (error: Error) => TCatch;
}

const API = {
  gouvServer: (): string => {
    return "https://data.education.gouv.fr/api/v2";
  },
  requestHelper: async <T, TCatch>(params: RequestParamsHelper<T, TCatch>): Promise<T | TCatch> => {
    try {
      const response = await fetch(params.url, params.requestOptions);
      return params.success(response);
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (error: any) {
      return params.failure(error);
    }
  },
  getSchoolList: async (): Promise<School[]> => {
    return API.requestHelper({
      name: "fetchSchoolList",
      url: `${API.gouvServer()}/catalog/datasets/fr-en-annuaire-education/records?where=nom_commune%3D%22Paris%22&order_by=nombre_d_eleves%20desc&limit=100&offset=0`,
      success: async data => {
        const records = readFromJSON(await data.json(), "records");
        let schools: School[] = [];
        if (Array.isArray(records)) {
          schools = records.map(record => new School(readFromJSON(record, "record")));
        }
        return schools;
      },
      failure: async error => {
        // TODO: Handle error
        console.error(error);
        return [new School()];
      },
    });
  },
};

export default API;
