import readFromJSON from "../helpers/json";

interface School {
  id: string;
  timestamp: number;
  size: number;
  fields: Fields;
}

interface Fields {
  type_rattachement_etablissement_mere?: unknown | null;
  lycee_agricole?: unknown | null;
  apprentissage?: unknown | null;
  voie_generale?: unknown | null;
  ministere_tutelle: string;
  statut_public_prive: string;
  libelle_region: string;
  telephone: string;
  date_maj_ligne: string;
  hebergement: number;
  section_europeenne?: unknown | null;
  libelle_nature: string;
  lycee_militaire?: unknown | null;
  ecole_elementaire: number;
  voie_technologique?: unknown | null;
  fiche_onisep?: unknown | null;
  etat: string;
  type_etablissement: string;
  rpi_concentre: number;
  identifiant_de_l_etablissement: string;
  code_region: string;
  ulis: number;
  restauration: number;
  ecole_maternelle: number;
  lycee_des_metiers?: unknown | null;
  date_ouverture: string;
  voie_professionnelle?: unknown | null;
  nombre_d_eleves: number;
  rpi_disperse?: unknown | null;
  coordy_origine: number;
  libelle_zone_animation_pedagogique: string;
  siren_siret: string;
  mail: string;
  type_contrat_prive: string;
  nom_commune: string;
  segpa?: unknown | null;
  adresse_3: string;
  adresse_2?: unknown | null;
  adresse_1: string;
  fax?: unknown | null;
  web?: unknown | null;
  greta?: unknown | null;
  code_nature: number;
  code_commune: string;
  latitude: number;
  position: Position;
  section_theatre?: unknown | null;
  section_internationale?: unknown | null;
  post_bac?: unknown | null;
  precision_localisation: string;
  etablissement_mere?: unknown | null;
  multi_uai: number;
  nom_circonscription: string;
  code_postal: string;
  libelle_departement: string;
  section_cinema?: unknown | null;
  code_academie: string;
  libelle_academie: string;
  appartenance_education_prioritaire?: unknown | null;
  longitude: number;
  nom_etablissement: string;
  section_arts?: unknown | null;
  epsg_origine: string;
  code_zone_animation_pedagogique: string;
  pial: string;
  section_sport?: unknown | null;
  code_departement: string;
  code_type_contrat_prive: string;
  coordx_origine: number;
}

interface Position {
  lat: number;
  lon: number;
}

class School {
  constructor(json?: { id: string; size: number; timestamp: number; fields: Fields }) {
    this.id = readFromJSON(json, "id", true) ?? "#unknown";
    if (this.id === "#unknown") {
      throw new Error("School ID can not be null");
    }
    this.size = readFromJSON(json, "size") ?? 0;
    this.timestamp = readFromJSON(json, "timestamp") ?? new Date().getTime();
    this.fields = readFromJSON(json, "fields") ?? ({} as Fields);
  }
}

export default School;
