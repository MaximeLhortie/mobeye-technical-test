/**
 * @description Used to generate JSON objects export that will be directly downloaded by browser
 * @param objects Objects list to export
 */
export function exportToJSON<T>(objects: T[]): void {
  if (!objects || !objects.length) {
    return;
  }
  const blob = new Blob([JSON.stringify(objects)], { type: "text/csv;charset=utf-8;" });
  const link = document.createElement("a");
  if (link.download !== undefined) {
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "export.json");
    link.style.visibility = "hidden";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}

/**
 * @description Used to generate CSV objects export that will be directly downloaded by browser
 * @param objects Objects list to export
 */
export function exportToCSV<T>(objects: T[]): void {
  if (!objects || !objects.length) {
    return;
  }
  const separator = ",";
  const keys = Object.keys(objects[0]);
  const csvData = `${keys.join(separator)}\n${objects
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    .map((row: any) => {
      return keys
        .map(k => {
          let cell = row[k] === null || row[k] === undefined ? "" : row[k];
          switch (true) {
            case cell instanceof Date:
              cell = cell.toLocaleString();
              break;
            case cell instanceof Object:
              cell = JSON.stringify(cell);
              break;
            default:
              cell = cell.toString().replace(/"/g, '""');
          }
          if (cell.search(/("|,|\n)/g) >= 0) {
            cell = `"${cell}"`;
          }
          return cell;
        })
        .join(separator);
    })
    .join("\n")}`;

  const blob = new Blob([csvData], { type: "text/csv;charset=utf-8;" });
  const link = document.createElement("a");
  if (link.download !== undefined) {
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "export.csv");
    link.style.visibility = "hidden";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}
